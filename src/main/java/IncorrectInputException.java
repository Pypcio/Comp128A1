/**
 * Created by Szymon Pypłacz on 2017-04-20.
 */
public class IncorrectInputException extends Exception{
    public IncorrectInputException(String message) {
        super(message +  " Try again.");
        System.out.println(this.getMessage());
    }
}
