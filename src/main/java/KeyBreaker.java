import java.util.ArrayList;
import java.util.List;

/**
 * Created by Szymon Pypłacz on 2017-03-26.
 */
public class KeyBreaker {

    public static boolean breakKey(ByteCode key) {
        String originalKeyByString = key.getByteStream();
        ByteCode rand = new ByteCode(16);
        System.out.println("Key: 0x" + key.getByteStream());
        System.out.println("Generated rand: " + rand.getByteStream());

        for (int i = 0; i < 8; i++) {
            String sres = fixedRand(key, rand, i);
            fixedKey(key, rand, i, sres);
        }
        System.out.println("Calculated key: " + key.getByteStream());
        if (key.getByteStream().equals(originalKeyByString)) {
            System.out.println("Copying finished with success");
            return true;
        }
        return false;
    }

    private static String fixedRand(ByteCode key, ByteCode rand, int i) {
        List<String> sresList = new ArrayList<>();
        for (int j = 0; j < 16; j++) {
            for (int k = 0; k < 16; k++) {
                for (int l = 0; l < 16; l++) {
                    for (int m = 0; m < 16; m++) {
                        changeByteCodeInIteration(rand, i, j, k, l, m);
                        String s = Comp128.comp128v1(key.getBytes(), rand.getBytes()).getByteStream();
                        if (sresList.contains(s)) {
                            System.out.println("Found collision in " + (i + 1) + " iteration with rand " + rand.getByteStream() + ", sres: " + s);
                            return s;
                        } else {
                            sresList.add(s);
                        }
                    }
                }
            }
        }
        return null;
    }

    private static ByteCode fixedKey(ByteCode key, ByteCode rand, int i, String sres) {
        for (int j = 0; j < 16; j++) {
            for (int k = 0; k < 16; k++) {
                for (int l = 0; l < 16; l++) {
                    for (int m = 0; m < 16; m++) {
                        changeByteCodeInIteration(key, i, j, k, l, m);
                        String s = Comp128.comp128v1(key.getBytes(), rand.getBytes()).getByteStream();
                        if (sres.equals(s)) {
                            System.out.println("Found collision in " + (i + 1) + " iteration with key " + key.getByteStream() + ", sres: " + s);
                            return key;
                        }
                    }
                }
            }
        }
        System.out.println("Key is unbroken");
        System.exit(0);
        return null;
    }

    private static void changeByteCodeInIteration(ByteCode bc, int i, int j, int k, int l, int m) {
        bc.changePositionForBreak(2 * i, j);
        bc.changePositionForBreak(2 * i + 1, k);
        bc.changePositionForBreak(2 * i + 16, l);
        bc.changePositionForBreak(2 * i + 17, m);
    }
}
