import java.util.Random;

/**
 * Created by Szymon Pypłacz on 2017-03-25.
 */
public class ByteCode {
    String byteStream;
    int[] bytes;

    public void setByteStream(String byteStream) {
        this.byteStream = byteStream;
    }

    public String getByteStream() {
        return byteStream;
    }

    public int[] getBytes() {
        return bytes;
    }


    public void setBytes(int[] bytes) {
        this.bytes = bytes;
    }

    public ByteCode(String byteStream) {
        this.byteStream = byteStream;
        int size = byteStream.length()/2;
        this.bytes = new int[size];
        for(int i = 0; i<size; i++){
            bytes[i] = Integer.decode("0x" + byteStream.substring(2*i, 2*i+2));
        }
    }

    public ByteCode(int size){
        this.byteStream = getRandomByte(size);
        this.bytes = new int[size];
        for(int i = 0; i<size; i++){
            bytes[i] = Short.decode("0x" + byteStream.substring(i, i+2));
        }
    }

    public ByteCode changePositionForBreak(int position, int value){
        String ch = translateIntToHex(value).toUpperCase();
        StringBuilder line = new StringBuilder(byteStream);
        line.setCharAt(position, ch.charAt(0));
        int[] newBytesArray = new int[16];
        this.setByteStream(line.toString());
        for(int i = 0; i<16; i++){
            newBytesArray[i] = Short.decode("0x" + line.toString().substring(2*i, 2*i+2));
        }
        this.setBytes(newBytesArray);
        return this;
    }

    public ByteCode(int[] bytes){
        this.bytes = bytes;
        this.byteStream = createByteStreamFromShortArray(bytes);
    }

    private String getRandomByte(int size) {
        Random random = new Random();
        String stream = "";
        for (int i = 0; i < size; i++) {
            int first = random.nextInt(16);
            int second = random.nextInt(16);
            stream = stream + translateIntToHex(first) + translateIntToHex(second);
        }
        return stream;
    }


    private String translateIntToHex(int value){
            return Integer.toHexString(value).toUpperCase();
    }

    private String createByteStreamFromShortArray(int[] array){
        String s = "";
        for(int i = 0; i<array.length; i++){
            s = s +translateByteToString(array[i]);
        }
        return s;
    }

    public String translateByteToString(int value){
        if(value < 16) {
            return "0" + Integer.toHexString(value).toUpperCase();
        }
        return Integer.toHexString(value).toUpperCase();
    }
}



