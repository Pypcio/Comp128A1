import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        ByteCode key;
        try {
            if (args.length == 1) {
                String value = Arrays.stream(args).findFirst().get();
                if (value.length() == 32) {
                    value = value.toUpperCase();
                } else if (value.length() == 34 && value.startsWith("0x")) {
                    value = value.substring(2).toUpperCase();
                } else {
                    throw new IncorrectInputException("There are incorrect length of key.");
                }
                key = new ByteCode(value);
                KeyBreaker.breakKey(key);
            } else {
                throw new IncorrectInputException("There are no input key value.");
            }
        } catch (IncorrectInputException e) {
            e.getMessage();
        }
    }

}